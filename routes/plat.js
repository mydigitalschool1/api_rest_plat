const router = require('express').Router();
const platController = require('../controllers/plat.controller.js');

router.route('/')
.get(platController.getAll);

router.route('/plats')
    // to create new resources
    .post(platController.create)
    // to retrieve resource
    .get(platController.getAll);
    /*.get(function(req, res, next) {
        // Respond with some data and return status OK
        userController.findAll();
        res.status(200).send();
    });*/

    router.route('/plats/add/:qte')
    .get(platController.add);

    router.route('/plats/stock/add')
    .put(platController.updateStockAdd);

    router.route('/plats/stock/minus')
    .put(platController.updateStockMinus);
    
    router.route('/plats/:platId')
     .get(platController.get)
     .put(platController.update)
     //.put(productController.updateStock)
     .delete(platController.deleteProduct);

     router.route('/plats/stock/:platId')
     .put(platController.updateStock);

 module.exports = router;