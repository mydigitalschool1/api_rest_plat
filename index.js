//import pool from './models/database.js';

'use strict'
/* eslint-env node */
/* eslint-disable no-console */

// Importer le paquet express
const express = require('express');
const mysql = require('mysql');
//const cors = require('cors');

//const port = process.env.PORT || 5000;
const port = 3000;

// Cree une application express
const app = express();

// Crée une route pour la page d'accueil '/'
app.get('/', (req, res) => {
  // Chemin à personnaliser !!!
  //res.sendFile('./views/index.html');
  res.sendFile(__dirname + '/views/index.html');
});

//https://raddy.dev/blog/using-node-js-with-mysql-crud-xampp-phpmyadmin/

//connect with database
const pool  = mysql.createPool({
  connectionLimit : 10,
  host            : 'localhost',
  user            : 'root',
  password        : '',
  database        : 'mds_api_plat'
});


//#region get

// Get all products
app.get('/plat', (req, res) => {
  pool.getConnection((err, connection) => {
      if(err) throw err
      console.log('connected as id ' + connection.threadId)
      connection.query('SELECT * from plat', (err, rows) => {
          connection.release() // return the connection to pool

          if (!err) {
              res.send(rows)
          } else {
              console.log(err)
          }

          // if(err) throw err
          console.log('The data from plat table are: \n', rows)
      })
  })
})

// Get a product
app.get('/plat/:id', (req, res) => {
  pool.getConnection((err, connection) => {
      if(err) throw err
      connection.query('SELECT * FROM plat WHERE id = ?', [req.params.id], (err, rows) => {
          connection.release() // return the connection to pool
          if (!err) {
              res.send(rows)
          } else {
              console.log(err)
          }
          
      })
  })
});


app.get('/api/plat', (req,res) => {
  // Récupère les paramètres de l'adresse
  const parametres = req.query
  // Récupère le paramètre 'termes'
  const termes = parametres.termes
  const produit = {id: 1, nom: "Tartiflette", qte: 2}; 
  res.send(produit);
});

// Créé route pour calculter le total d'une addition
// Route : /api/addition?termes=5.5,2,3
app.get('/api/params', (req, res) => {
  // Récupère les paramètres de l'adresse
  const parametres = req.query
  // Récupère le paramètre 'termes'
  const termes = parametres.termes

  if (termes == null) {
    res.sendStatus(400)
    return
  }
  res.send(termes);
});

//#endregion

const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

//#region post

// Créer une route POST /api/addition
// Le contenu de la requête est au format :
// { terms: [1, 2, 3, ...] }
app.post('/api/addition',
  jsonParser,
  (req, res) => {
    // Récupère le contenu de la requête
    const contenu = req.body
    // Récupère le paramètre 'termes'
    const termes = contenu.termes

    if (termes == null) {
      res.sendStatus(400)
      return
    }

    const total = termes
      // Convertie chaque élément du tableau en nombre
      .map(element => Number.parseFloat(element))
      // Retire les éléments qui ne sont pas nombres
      .filter(element => Number.isNaN(element) === false)
      // Additione tous les éléments
      .reduce((element, acc) => acc + element, 0)

    res.send({ resultat: total })
  })



//#endregion

//#region put

//#endregion

//#region delete
app.delete('/:id', (req, res)=>{
    
})

// Delete a beer
// app.delete('/:id', (req, res) => {

//   pool.getConnection((err, connection) => {
//       if(err) throw err
//       connection.query('DELETE FROM beers WHERE id = ?', [req.params.id], (err, rows) => {
//           connection.release() // return the connection to pool
//           if (!err) {
//               res.send(`Beer with the record ID ${[req.params.id]} has been removed.`)
//           } else {
//               console.log(err)
//           }
          
//           console.log('The data from beer table are: \n', rows)
//       })
//   })
// });

//#endregion

app.listen(port, ()=>{
    console.log('server is up !');
});